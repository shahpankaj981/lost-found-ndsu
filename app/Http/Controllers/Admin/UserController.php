<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\FoundItemRequest;
use App\Models\Item\BaseItem;
use App\Models\Item\FoundItem;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\DataTables;

class UserController extends Controller
{
    public function __construct(DataTables $dataTables)
    {
        $this->dataTables = $dataTables;
    }

    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            $query = app(User::class)
                ->select('name', 'email', 'phone', 'id');

            return $this->dataTables::of($query)
//                ->addColumn('action', function ($user) {
//                    $actions = "<a href='".route('user.edit', $user->id)."' class='btn btn-sm btn-pill btn-outline-primary' title='Edit User'> <i class='fa fa-pen'></i> </a>";
//                    $actions .= "&nbsp; <a href='".route('user.edit', $user->id)."' class='btn btn-sm btn-pill btn-outline-danger' title='Delete User'> <i class='fa fa-trash'></i> </a>";
//
//                    return $actions;
//                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('admin.user.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('admin.user.create');
    }

    public function getUserByEmail($email)
    {
        $user = app(User::class)->whereEmail($email)->first();
        if(!$user) {
            return \response()->json(['success' => false, 'message' => 'User Not Found!!', 'data' =>[]]);
        }

        return \response()->json(['success' => true, 'message' => 'User Found!!', 'data' =>['name' => $user->name, 'phone' => $user->phone]]);
    }
}
