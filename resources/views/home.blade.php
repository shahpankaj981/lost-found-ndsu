@extends('layouts.master')

@php($title='Dashboard')
@section('actions')
    <a href="{{ route('found-item.create') }}" class="btn btn-sm btn-success">Add New &nbsp; <i class="fa fa-plus"></i> </a>
@endsection
@section('content')
    <div class="accordion accordion-solid accordion-toggle-plus" id="accordionExample6">
        <div class="card">
            <div class="card-header" id="headingTwo6">
                <div class="card-title" data-toggle="collapse" data-target="#lost-items" aria-expanded="true">
                    <i class="flaticon-technology-1"></i>My Lost Items</div>
            </div>
            <div id="lost-items" class="collapse show" data-parent="#accordionExample6" style="">
                <div class="card-body">
                    <table id="lost-item-table" class="table table-borderless">
                        <thead>
                        <tr>
                            <th>Item Name</th>
                            <th>Lost Place</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="card">
            <div class="card-header" id="headingOne6">
                <div class="card-title collapsed" data-toggle="collapse" data-target="#found-items" aria-expanded="false">
                    <i class="flaticon-technology-1"></i>My Found Items</div>
            </div>
            <div id="found-items" class="collapse" data-parent="#accordionExample6" style="">
                <div class="card-body">
                    <table id="found-item-table" class="table table-borderless">
                        <thead>
                        <tr>
                            <th>Item Name</th>
                            <th>Found Place</th>
                            <th>Date</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lost-item-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{{ route("lost-item.index", ['my_items' => 1]) }}',
                order: [],
                stateSaveParams: function(settings, data) {
                    data.order = [];
                },
                columns: [
                    {data: 'name', name:'base_items.name'},
                    {data: 'place', name:'base_items.place'},
                    {data: 'date', name: 'base_items.date'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });


            $('#found-item-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{{ route("found-item.index", ['my_items' => 1]) }}',
                order: [],
                stateSaveParams: function(settings, data) {
                    data.order = [];
                },
                columns: [
                    {data: 'name', name:'base_items.name'},
                    {data: 'place', name:'base_items.place'},
                    {data: 'date', name: 'base_items.date'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });
        });
    </script>
@endpush
