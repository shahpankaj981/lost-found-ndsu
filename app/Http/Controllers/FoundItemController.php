<?php

namespace App\Http\Controllers;

use App\Http\Requests\FoundItemRequest;
use App\Http\Requests\UserRequest;
use App\Imports\FoundItemsImport;
use App\Imports\LostItemsImport;
use App\Models\Item\BaseItem;
use App\Models\Item\FoundItem;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class FoundItemController extends Controller
{
    /**
     * @var DataTables
     */
    public $dataTables;

    public function __construct(DataTables $dataTables)
    {
        $this->middleware('auth')->except('index');
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            $query = app(FoundItem::class)
                ->join('base_items', 'found_items.parent_item_id', '=', 'base_items.id')
                ->join('users', 'base_items.user_id', '=', 'users.id')
                ->select('users.name as user_name', 'base_items.name as name', 'place', 'date',
                    'found_items.id as id', DB::raw('IF(found_items.claimed_by is null, "", "Claimed") as status'));

            if($name = $request->get('filter_name')) {
                $query->where('base_items.name', 'like', "%{$name}%");
            }
            if($place = $request->get('filter_place')) {
                $query->where('place', 'like', "%{$place}%");
            }
            if($date = $request->get('filter_date')) {
                $query->whereDate('base_items.date', $date);
            }
            if($user = $request->get('filter_user')) {
                $query->where('users.name', 'like', "%{$user}%")
                    ->orWhere('users.email', 'like', "%{$user}%");
            }
            if($myItem = $request->get('my_items')) {
                $query = $query->where('user_id', auth()->id());
            }
            if (!request()->has('order')) {
                $query = $query->orderBy('base_items.created_at', 'desc');
            }

            return $this->dataTables::of($query)
                ->addColumn('action', function ($item) use ($myItem) {
                    $actions =  "<a href='".route('found-item.show', $item->id)."' class='btn btn-xs pl-2 pr-1 pt-1 pb-1 btn-outline-info' title='View Item'> <i class='fa fa-eye'></i> </a>";
                    if($myItem | isAdmin()) {
                        $actions .= "&nbsp; <a href='".route('found-item.edit', $item->id)."' class='btn btn-xs pl-2 pr-1 pt-1 pb-1 btn-outline-success' title='Edit Item'> <i class='fa fa-pen'></i> </a>";
                    }
                    if(isAdmin() && !$item->status) {
                        $actions .= "&nbsp; <a href='javascript:void' onclick='handleMarkAsClaim({$item->id})' class='btn btn-xs pl-2 pr-1 pt-1 pb-1 btn-outline-primary' title='Mark As Claimed' data-toggle='modal' data-target='#mark-claim-modal' data-itemid='{$item->id}'> <i class='fa fa-check'></i> </a>";
                    }

                    return $actions;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('item.found.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('item.found.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FoundItemRequest $request
     * @return RedirectResponse
     */
    public function store(FoundItemRequest $request)
    {
        $data = $request->all();
        try {
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->file('image'), 'lost_items', null, true);
            }
            DB::beginTransaction();
            $data['user_id'] = auth()->id();
            $baseItem        = app(BaseItem::class)->create($data);
            $baseItem->foundItem()->create([]);
            DB::commit();

            flash('Found Item Created Successfully!!')->success();
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Something went wrong!!')->error();
        }

        return redirect()->route('found-item.show', $baseItem->foundItem);
    }

    /**
     * Display the specified resource.
     *
     * @param FoundItem $foundItem
     * @return Application|Factory|View|Response
     */
    public function show(FoundItem $foundItem)
    {
        $baseItem = $foundItem->baseItem;

        return view('item.found.show', compact('baseItem', 'foundItem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FoundItem $foundItem
     * @return Application|Factory|View|Response
     */
    public function edit(FoundItem $foundItem)
    {
        $baseItem = $foundItem->baseItem;

        return view('item.found.edit', compact('baseItem', 'foundItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FoundItemRequest $request
     * @param FoundItem        $foundItem
     * @return RedirectResponse
     */
    public function update(FoundItemRequest $request, FoundItem $foundItem)
    {
        $data = $request->only('name', 'place', 'date', 'colour', 'size', 'description');
        try {
            DB::beginTransaction();
            $foundItem->baseItem()->update($data);
            DB::commit();

            flash('Found Item Updated Successfully!!')->success();
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Something went wrong!!')->error();
        }

        return redirect()->route('found-item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\FoundItem $foundItem
     * @return RedirectResponse
     */
    public function destroy(FoundItem $foundItem)
    {
        try {
            $foundItem->baseItem()->delete();

            flash('Found Item Deleted Successfully!!')->success();
        } catch (Exception $exception) {
            flash('Something went wrong!!')->error();
        }

        return redirect()->route('found-item.index');
    }

    public function importData(Request $request)
    {
        Excel::import(new FoundItemsImport, $request->file('file'));
        flash('Found Items Data imported successfully!!')->success();

        return redirect()->back();
    }

    public function markAsClaimed(FoundItem $foundItem, Request $request)
    {
       $data =$request->all();
       $user = app(User::class)->updateOrCreate(['email' => $data['email']], ['name' => $data['name'], 'phone' => $data['phone'], 'password' => Hash::make('password')]);

       $foundItem->update(['claimed_by' => $user->id]);
       flash('Marked as Claimed!!')->success();

       return redirect()->back();
    }
}
