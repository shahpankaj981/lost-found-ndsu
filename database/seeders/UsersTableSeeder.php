<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app(User::class)->updateOrCreate([
            'name'     => 'Admin',
            'email'    => 'admin@admin.com',
            'role'     => 'admin',
        ],
        [
            'phone'    => '000000000',
            'password' => Hash::make('password'),
        ]);
    }
}
