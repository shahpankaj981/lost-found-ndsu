<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class FoundItem extends BaseItem
{
    use HasFactory;

    protected $fillable = [
        'claimed_by'
    ];

    public function baseItem(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(BaseItem::class, 'parent_item_id');
    }
}
