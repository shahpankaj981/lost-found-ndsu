@extends('layouts.master')

@php
    $title='Create Lost Item';
    $errors->setFormat('<span class="text-danger" style="font-weight:small;">:message</span>');
@endphp
@section('actions')
    <a href="{{ route('lost-item.index') }}" class="btn btn-secondary"> <i class="fa fa-backward"></i> &nbsp; Back</a>
@endsection
@section('content')
    {!! Form::open(['route' => 'lost-item.store', 'method' => 'post', 'files' => true]) !!}
    @include('item.lost.partials._form')
    {!! Form::close() !!}
@endsection
