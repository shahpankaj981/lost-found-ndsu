<?php

namespace App\Http\Controllers;

use App\Http\Requests\LostItemRequest;
use App\Imports\LostItemsImport;
use App\Models\Item\BaseItem;
use App\Models\Item\LostItem;
use Carbon\Carbon;
use Exception;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\DataTables;

class LostItemController extends Controller
{
    /**
     * @var DataTables
     */
    public $dataTables;

    public function __construct(DataTables $dataTables)
    {
        $this->middleware('auth')->except('index');
        $this->dataTables = $dataTables;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|Response
     */
    public function index(Request $request)
    {
        if ($request->expectsJson()) {
            $query = app(LostItem::class)
                ->join('base_items', 'lost_items.parent_item_id', '=', 'base_items.id')
                ->join('users', 'base_items.user_id', '=', 'users.id')
                ->select('users.name as user_name', 'base_items.name as name', 'place', 'date', 'lost_items.id as id');

            if($name = $request->get('filter_name')) {
                $query->where('base_items.name', 'like', "%{$name}%");
            }
            if($place = $request->get('filter_place')) {
                $query->where('place', 'like', "%{$place}%");
            }
            if($date = $request->get('filter_date')) {
                $query->whereDate('base_items.date', $date);
            }
            if($user = $request->get('filter_user')) {
                $query->where('users.name', 'like', "%{$user}%")
                    ->orWhere('users.email', 'like', "%{$user}%");
            }

            if($myItem = $request->get('my_items')) {
                $query = $query->where('user_id', auth()->id());
            }
            if (!request()->has('order')) {
                $query = $query->orderBy('base_items.created_at', 'desc');
            }
            return $this->dataTables::of($query)
                ->addColumn('action', function ($item) use($myItem) {
                    $actions = "<a href='".route('lost-item.show', $item->id)."' class='btn btn-xs pl-2 pr-1 pt-1 pb-1 btn-outline-info' type='button' title='View Item'> <i class='fa fa-eye'></i> </a>";

                    if($myItem) {
                        $actions .= "&nbsp; <a href='".route('found-item.edit', $item->id)."' class='btn btn-xs pl-2 pr-1 pt-1 pb-1 btn-outline-success' title='Edit Item'> <i class='fa fa-pen'></i> </a>";
                    }

                    return $actions;
                })
                ->rawColumns(['action'])
                ->make(true);
        }

        return view('item.lost.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|Response
     */
    public function create()
    {
        return view('item.lost.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(LostItemRequest $request)
    {
        $data = $request->all();
        try {
            if($request->hasFile('image')) {
                $data['image'] = uploadFile($request->file('image'), 'lost_items', null, true);
            }
            DB::beginTransaction();
            $data['user_id'] = auth()->id();
            $baseItem        = app(BaseItem::class)->create($data);
            $baseItem->lostItem()->create([]);
            DB::commit();

            flash('Lost Item Created Successfully!!')->success();
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Something went wrong!!')->error();
        }

        return redirect()->route('lost-item.show', $baseItem->lostItem);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\LostItem $lostItem
     * @return Application|Factory|View|Response
     */
    public function show(LostItem $lostItem)
    {
        $baseItem = $lostItem->baseItem;

        return view('item.lost.show', compact('baseItem', 'lostItem'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param LostItem $lostItem
     * @return Application|Factory|View|Response
     */
    public function edit(LostItem $lostItem)
    {
        $baseItem = $lostItem->baseItem;

        return view('item.lost.edit', compact('baseItem', 'lostItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param LostItemRequest $request
     * @param LostItem        $lostItem
     * @return RedirectResponse
     */
    public function update(LostItemRequest $request, LostItem $lostItem)
    {
        $data = $request->only('name', 'place', 'date', 'colour', 'size', 'description');
        try {
            DB::beginTransaction();
            $lostItem->baseItem()->update($data);
            DB::commit();

            flash('Lost Item Updated Successfully!!')->success();
        } catch (Exception $exception) {
            DB::rollBack();
            flash('Something went wrong!!')->error();
        }

        return redirect()->route('lost-item.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\LostItem $lostItem
     * @return RedirectResponse
     */
    public function destroy(LostItem $lostItem)
    {
        try {
            $lostItem->baseItem()->delete();

            flash('Lost Item Deleted Successfully!!')->success();
        } catch (Exception $exception) {
            flash('Something went wrong!!')->error();
        }

        return redirect()->route('lost-item.index');
    }

    public function importData(Request $request)
    {
        Excel::import(new LostItemsImport, $request->file('file'));
        flash('Lost Items Data imported successfully!!')->success();

        return redirect()->back();
    }
}
