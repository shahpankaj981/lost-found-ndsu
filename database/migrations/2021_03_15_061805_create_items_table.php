<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('base_items', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('place');
            $table->date('date');
            $table->string('colour')->nullable();
            $table->string('size')->nullable();
            $table->text('description')->nullable();
            $table->string('image')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->timestamps();
            $table->foreign('user_id')
                  ->references('id')
                  ->on('users')
                  ->onDelete('cascade');
        });

        Schema::create('lost_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parent_item_id');
            $table->string('returned_by')->nullable();
            $table->timestamps();
            $table->foreign('parent_item_id')
                ->references('id')
                ->on('base_items')
                ->onDelete('cascade');
        });

        Schema::create('found_items', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('parent_item_id');
            $table->string('claimed_by')->nullable();
            $table->timestamps();
            $table->foreign('parent_item_id')
                ->references('id')
                ->on('base_items')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lost_items');
        Schema::dropIfExists('found_items');
        Schema::dropIfExists('base_items');
    }
}
