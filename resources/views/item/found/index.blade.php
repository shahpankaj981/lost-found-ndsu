@extends('layouts.master')

@php($title='Found Item List')
@section('actions')
    @if(auth()->user() && auth()->user()->role == 'admin')
        <button type="button" class="btn btn-sm btn-primary" data-toggle="modal" data-target="#import-data-modal">
            Import Data &nbsp; <i class="fa fa-upload"></i>
        </button>
        &nbsp;
    @endif
   <a href="{{ route('found-item.create') }}" class="btn btn-sm btn-success">Add New &nbsp; <i class="fa fa-plus"></i> </a>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <h3>Filter Found Items</h3>
        </div>
    </div>
    <div class="row form-group">
        <div class="col-md-3">
            <label>Item Name</label>
            <input type="text" class="form-control" id="filter-item-name" placeholder="Filter Item Name">
        </div>
        <div class="col-md-3">
            <label>Lost Place</label>
            <input type="text" class="form-control" id="filter-place" placeholder="Filter Place">
        </div>
        <div class="col-md-3">
            <label>Lost Date</label>
            <input type="date" class="form-control" id="filter-date" placeholder="Filter Date">
        </div>
        <div class="col-md-3">
            <label>User</label>
            <input type="text" class="form-control" id="filter-user" placeholder="Filter user name, email">
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <button id="btn-filter-items" type="button" class="btn btn-sm btn-success btn-block">Filter Items &nbsp; <i class="fa fa-search"></i> </button>
        </div>
    </div>
    <hr>

   <div class="row">
       <div class="col-md-12">
           <table id="found-item-table" class="table table-borderless">
               <thead>
               <tr>
                   <th>Item Name</th>
                   <th>Found Place</th>
                   <th>Date</th>
                   <th>Status</th>
                   <th>User</th>
                   <th>Actions</th>
               </tr>
               </thead>
               <tbody></tbody>
           </table>
       </div>
   </div>

    <div class="modal fade" id="import-data-modal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Import Data From Excel</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                {!! Form::open(['route' => 'found-item.import', 'method' => 'post', 'files' => true]) !!}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{ route('download-sample-file') }}" type="button" class="btn btn-sm btn-success btn-block">Download Sample File &nbsp; <i class="fa fa-download"></i> </a>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::label('file', 'File', ['class' => 'control-label']) !!}
                            {!! Form::file('file', ['required' => true, 'class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Import</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    <div class="modal fade" id="mark-claim-modal" data-backdrop="static" tabindex="-1" role="dialog"
         aria-labelledby="staticBackdrop" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Mark as Claim</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i aria-hidden="true" class="ki ki-close"></i>
                    </button>
                </div>
                {!! Form::open(['method' => 'post', 'id' => 'claim-form']) !!}
                <div class="modal-body">
                    <h3 class="font-size-lg text-dark font-weight-bold mb-6">User Info:</h3>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            {!! Form::label('email', 'Email', ['class' => 'control-label']) !!}
                            {!! Form::email('email', null, ['id' => 'claim-email', 'required' => true, 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            {!! Form::label('name', 'Name', ['class' => 'control-label']) !!}
                            {!! Form::text('name', null, ['id' => 'claim-name','required' => true, 'class' => 'form-control']) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 form-group">
                            {!! Form::label('phone', 'Phone', ['class' => 'control-label']) !!}
                            {!! Form::text('phone', null, ['id' => 'claim-phone','required' => true, 'class' => 'form-control']) !!}
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Close
                    </button>
                    <button type="submit" class="btn btn-primary font-weight-bold">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection


@push('scripts')
    <script type="text/javascript">
        let baseUrl = "{{ route('found-item.index') }}";
        function handleMarkAsClaim(itemId)
        {
            $('#claim-form').attr('action', `/admin/found-item/${itemId}/mark-claim`)
        }

        $(document).ready(function () {
            let dt1 = $('#found-item-table').DataTable({
                "processing": true,
                "serverSide": true,
                "searching": false,
                order: [],
                stateSaveParams: function(settings, data) {
                    data.order = [];
                },
                "ajax": {
                    url: "{{ route("found-item.index") }}",
                    data: function (d) {
                        d.filter_name = $('#filter-item-name').val();
                        d.filter_place = $('#filter-place').val();
                        d.filter_date = $('#filter-date').val();
                        d.filter_user = $('#filter-user').val();
                    }
                },
                columns: [
                    {data: 'name', name:'base_items.name'},
                    {data: 'place', name:'base_items.place'},
                    {data: 'date', name: 'base_items.date'},
                    {data: 'status', name: 'status', searchable:false, orderable: false},
                    {data: 'user_name', name:'users.name'},
                    {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });
            $('#btn-filter-items').on('click', function() {
                dt1.draw();
            })
            $("#claim-email").on('change', function(e) {
                let email = e.target.value;
                if(email) {
                    $.ajax({
                        url: `/admin/user/info-by-email/${email}`,
                    }).done(function(response) {
                        console.log(response)
                        if(response.success) {
                            $("#claim-name").val(response.data.name);
                            $("#claim-phone").val(response.data.phone);
                        } else {
                            $("#claim-name").val('');
                            $("#claim-phone").val('');
                        }
                    });
                }
            })
        });
    </script>
@endpush
