<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/',  function (){
    if(auth()->user()) {
        return redirect()->route('dashboard');
    } else {
        return redirect()->route('lost-item.index');
    }
});

Route::get('/dashboard',  [App\Http\Controllers\HomeController::class, 'index'])->name('dashboard');

Auth::routes();

Route::get('/home', function(){
    return view('index');
})->name('home');

Route::resource('lost-item', 'App\Http\Controllers\LostItemController');
Route::resource('found-item', 'App\Http\Controllers\FoundItemController');

Route::group(['middleware' => ['auth', 'admin'], 'prefix' => 'admin'], function () {
    Route::get('user/info-by-email/{email}', 'App\Http\Controllers\Admin\UserController@getUserByEmail')->name('user.get-by-email');
    Route::post('found-item/{found_item}/mark-claim', 'App\Http\Controllers\FoundItemController@markAsClaimed')->name('found-item.mark-claim');

    Route::get('download-sample-file', 'App\Http\Controllers\HomeController@downloadSampleFile')->name('download-sample-file');
    Route::post('lost-item/import', 'App\Http\Controllers\LostItemController@importData')->name('lost-item.import');
    Route::post('found-item/import', 'App\Http\Controllers\FoundItemController@importData')->name('found-item.import');
    Route::resource('user', 'App\Http\Controllers\Admin\UserController')->only('index');
});
