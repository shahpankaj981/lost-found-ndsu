<?php

namespace App\Imports;

use App\Models\Item\BaseItem;
use App\Models\Item\LostItem;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use PhpOffice\PhpSpreadsheet\Shared\Date;

class FoundItemsImport implements ToCollection, WithHeadingRow
{

    /**
     * @param Collection $collection
     */
    public function collection(Collection $rows)
    {
        $adminUser = app(User::class)->whereEmail('admin@admin.com')->first();
        foreach ($rows as $row) {
            $user = null;
            if (!empty($row['user_name']) && !empty($row['user_email'])) {
                $userData = [
                    'name'     => $row['user_name'],
                    'email'    => $row['user_email'],
                    'phone'    => $row['user_phone'],
                    'password' => Hash::make('password'),
                ];
                $user     = app(User::class)->updateOrCreate(['email' => $row['user_email']], $userData);
            }
            $itemData = [
                'name'        => $row['name'],
                'place'       => $row['place'],
                'date'        => Date::excelToDateTimeObject($row['date'])->format('Y-m-d'),
                'description' => $row['description'],
                'user_id'     => !empty($user) ? $user->id : $adminUser->id,
            ];
            $baseItem = app(BaseItem::class)->create($itemData);
            $baseItem->foundItem()->create([]);
        }
    }
}
