@extends('layouts.master')

@php
    $title='Create Found Item';
    $errors->setFormat('<span class="text-danger" style="font-weight:small;">:message</span>');
@endphp
@section('actions')
    <a href="{{ route('found-item.index') }}" class="btn btn-secondary"> <i class="fa fa-backward"></i> &nbsp; Back</a>
@endsection
@section('content')
    {!! Form::open(['route' => 'found-item.store', 'method' => 'post', 'files' => true]) !!}
    @include('item.found.partials._form')
    {!! Form::close() !!}
@endsection
