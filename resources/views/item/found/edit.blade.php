@extends('layouts.master')

@php
    $title='Edit Found Item';
    $errors->setFormat('<span class="text-danger" style="font-weight:small;">:message</span>');
@endphp
@section('actions')
    <a href="{{ route('found-item.index') }}" class="btn btn-secondary"> <i class="fa fa-backward"></i> &nbsp; Back</a>
@endsection
@section('content')
    {!! Form::model($baseItem, ['route' => ['found-item.update', $foundItem], 'method' => 'patch', 'files' => true]) !!}
    <div class="form-group">
        <label>Item Name *</label>
        {!! Form::text('name', null, ['class' => 'form-control', "placeholder"=>"Enter item name"]) !!}
        {!! $errors->first('name') !!}
    </div>
    <div class="form-group row">
        <div class="col-md-6">
            <label>Place *</label>
            {!! Form::text('place', null, ['class' => 'form-control', "placeholder"=>"Enter place where it was found"]) !!}
            {!! $errors->first('place') !!}
        </div>
        <div class="col-md-6">
            <label>Date</label>
            {!! Form::date('date', null, ['class' => 'form-control', "placeholder"=>"Enter date when it was found"]) !!}
            {!! $errors->first('date') !!}
        </div>
    </div>
    <div class="form-group row">
        <div class="col-md-6">
            <label>Size</label>
            {!! Form::text('size', null, ['class' => 'form-control', "placeholder"=>"Enter size"]) !!}
            {!! $errors->first('size') !!}
        </div>
        <div class="col-md-6">
            <label>Colour</label>
            {!! Form::text('colour', null, ['class' => 'form-control', "placeholder"=>"Enter colour"]) !!}
            {!! $errors->first('colour') !!}
        </div>
    </div>
    <div class="form-group">
        <label>Description</label>
        {!! Form::textarea('description', null, ['class' => 'form-control', "placeholder"=>"Describe the item..."]) !!}
        {!! $errors->first('description') !!}
    </div>
    <div class="form-group">
        <label>Image</label>
        {!! Form::file('image', ['class' => 'form-control']) !!}
        {!! $errors->first('image') !!}
    </div>

    <div class="card-footer">
        <button type="submit" class="btn btn-primary mr-2"><i class="fa fa-save"></i>&nbsp; Save</button>
        <a href="{{ route('found-item.index') }}" class="btn btn-secondary"> <i class="fa fa-backward"></i> &nbsp; Back</a>
    </div>

    {!! Form::close() !!}
@endsection
