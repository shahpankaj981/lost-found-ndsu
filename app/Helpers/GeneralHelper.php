<?php


use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;


function uploadFile($file, $path, $fileName = null, $isImage = true)
{
    $onlyName = $file->getClientOriginalName();
    $onlyName = pathinfo($onlyName, PATHINFO_FILENAME);
    $fileName = $fileName ? $fileName.time().'.' : rand().time().'.'.$file->getClientOriginalExtension();

    $savedFileName = Storage::putFileAs($path, $file, $fileName);

    if ($isImage) {
        Image::make($file)->resize(300, null, function ($constraint) {
            $constraint->aspectRatio();
        })->save();

        $file->storeAs(
            $path.'/thumbnail', $fileName
        );
    }

    return $savedFileName;
}

/**
 * Get storage file
 *
 * @param       $file
 * @param false $thumbnail
 * @return string
 */
function getFile($file, $thumbnail = false)
{
    if (!$file) {
        return asset('assets/media/misc/placeholder.png');
    }

    if ($thumbnail) {
        $path  = explode('/', $file);
        $new   = ['thumbnail'];
        $count = count($path);
        array_splice($path, $count - 1, 0, $new);

        return Storage::url(implode('/', $path));
    }

    return Storage::url($file);
}

function getLoggedInUser($attribute = null)
{
    if ($attribute) {
        return auth()->user()->{$attribute};
    }

    return auth()->user();
}

function getLoggedInUserName()
{
    $user = auth()->user();
    if (!$user) {
        return 'N/A';
    }

    return ucwords($user->name);
}

function getLoggedInUserId()
{
    return auth()->id();
}

function isAdmin()
{
    $loggedInUser = getLoggedInUser();
    if(!$loggedInUser) {
        return false;
    }

    return $loggedInUser->role == 'admin';
}

