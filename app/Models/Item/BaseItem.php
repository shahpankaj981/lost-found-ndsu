<?php

namespace App\Models\Item;

use App\Models\Item\FoundItem;
use App\Models\Item\LostItem;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BaseItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'place',
        'date',
        'colour',
        'size',
        'description',
        'image',
        'user_id',
    ];

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
//    protected static function booted()
//    {
//        static::creating(function ($item) {
//            $item->user_id = auth()->id();
//        });
//    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function lostItem(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(LostItem::class, 'parent_item_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function foundItem(): \Illuminate\Database\Eloquent\Relations\HasOne
    {
        return $this->hasOne(FoundItem::class, 'parent_item_id');
    }

    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class);
    }
}
