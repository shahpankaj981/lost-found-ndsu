@extends('layouts.master')

@php($title='Found Item Detail')
@section('actions')
    <a href="{{ route('found-item.index') }}" class="btn btn-secondary"> <i class="fa fa-backward"></i> &nbsp; Back</a>
@endsection
@section('content')
    <form class="form" id="kt_form">
        <h3 class="text-dark font-weight-bold mb-10">Item Details:</h3>
        <div class="row">
            <div class="col-md-9">
                <div class="my-5">
                    <div class="form-group row">
                        <label class="col-3">Item Name</label>
                        <div class="col-9">
                            <input class="form-control" disabled type="text" value="{{ $baseItem->name }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3">Found Place</label>
                        <div class="col-9">
                            <input class="form-control" disabled type="text" value="{{ $baseItem->place }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3">Found Date</label>
                        <div class="col-9">
                            <input class="form-control" disabled type="text" value="{{ $baseItem->date }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3">Item Size</label>
                        <div class="col-9">
                            <input class="form-control" disabled type="text" value="{{ $baseItem->size }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3">Item Colour</label>
                        <div class="col-9">
                            <input class="form-control" disabled type="text" value="{{ $baseItem->colour }}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3">Description</label>
                        <div class="col-9">
                            <textarea rows="5" class="form-control" disabled>{{ $baseItem->description }}</textarea>
{{--                            <input class="form-control" disabled type="text" value="{{ $baseItem->place }}">--}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3">Item Image</label>
                        <div class="col-9">
                            <input class="form-control" disabled type="text" value="{{ $baseItem->place }}">
                        </div>
                    </div>
                </div>
                <div class="separator separator-dashed my-10"></div>
                <div class="my-5">
                    <h3 class="text-dark font-weight-bold mb-10">User Details:</h3>
                    <div class="form-group row">
                        <label class="col-3">Name</label>
                        <div class="col-9">
                            <input class="form-control" disabled type="text" value="{{ $baseItem->user->name }}">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-3">Contact Phone</label>
                        <div class="col-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
																		<span class="input-group-text">
																			<i class="la la-phone"></i>
																		</span>
                                </div>
                                <input type="text" class="form-control" disabled value="{{ $baseItem->user->phone }}" placeholder="Phone">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-3">Email Address</label>
                        <div class="col-9">
                            <div class="input-group">
                                <div class="input-group-prepend">
																		<span class="input-group-text">
																			<i class="la la-at"></i>
																		</span>
                                </div>
                                <input type="text" class="form-control" disabled value="{{ $baseItem->user->email }}" placeholder="Email">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection
