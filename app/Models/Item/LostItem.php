<?php

namespace App\Models\Item;

use Illuminate\Database\Eloquent\Factories\HasFactory;

class LostItem extends BaseItem
{
    use HasFactory;

    protected $fillable=[
        'returned_by',

    ];

    public function baseItem(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(BaseItem::class, 'parent_item_id');
    }
}
