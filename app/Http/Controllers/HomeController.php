<?php

namespace App\Http\Controllers;

use App\Models\Item\LostItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function downloadSampleFile(): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        return response()->download(public_path('files/sample_file.xlsx'));
    }
}
