@extends('layouts.master')

@php($title='User List')
@section('actions')
{{--   <a href="{{ route('user.create') }}" class="btn btn-sm btn-success">Add New &nbsp; <i class="fa fa-plus"></i> </a>--}}
@endsection
@section('content')
    <table id="user-table" class="table table-borderless">
        <thead>
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Phone</th>
{{--            <th>Actions</th>--}}
        </tr>
        </thead>
        <tbody></tbody>
    </table>
@endsection


@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#user-table').DataTable({
                "processing": true,
                "serverSide": true,
                "ajax": '{{ route("user.index") }}',
                columns: [
                    {data: 'name', name:'name'},
                    {data: 'email', name:'email'},
                    {data: 'phone', name: 'phone'},
                    // {data: 'action', name: 'action', orderable: false, searchable: false}
                ],
            });
        });
    </script>
@endpush
